from app import app, db, bcrypt, login_manager
from flask import render_template, url_for, request, redirect, flash
from app.model import Category, Post, Admin, Comment, Navigation
from flask_login import login_user, current_user, logout_user, login_required
from datetime import datetime
from app.admin_routes.func import prevPost, nextPost


#  front page routing
@app.route('/')
def home():
    title = 'MedInPrint'
    query = Post.query.order_by(Post.post_id.desc())
    posts = query.paginate(per_page=7, error_out=True)

    prev_url = url_for('home', page=posts.prev_num)
    next_url = url_for('home', page=posts.next_num)

    return render_template('public/index.html', title=title, posts=posts,
                            prev_url=prev_url, next_url=next_url)


@app.route('/-<post_url>/', methods=["GET", "POST"])
def post(post_url):
    # processing comments by users and guest
    if request.method == "POST":
        rec = request.form
        name = rec["name"]
        email = rec["email"]
        content = rec["content"]
        post_id = rec["post_id"]
        date = datetime.now()

        # processing the add comment to post
        comment = Comment(name, email, content, post_id, date)
        db.session.add(comment)
        db.session.commit()
        return redirect(request.url)

    
    query = Post.query.filter_by(post_url=post_url).first()
    post_comment = Comment.query.filter_by(post_id=query.post_id)
    post_comment_count=post_comment.count()
    comments = post_comment.order_by(Comment.id.desc())
    title = query.post_title

    # processing the next post feature
    next_art = nextPost(Post, query.post_id)
    prev_art = prevPost(Post, query.post_id)

    return render_template('public/post.html', title=title, post=query,
                           comments=comments, next_art=next_art,
                           prev_art=prev_art,
                           post_comment_count=post_comment_count)


@app.route('/about/')
def about():
    title = 'About'
    return render_template('public/about.html', title=title)


@app.route('/<nav_url>/')
def navigationBar(nav_url):
    nav = Navigation.query.filter_by(nav_url=nav_url).first()
    n_url = nav.nav_url
    query = Post.query.filter_by(post_nav=nav.nav_id).order_by(Post.post_id.desc())
    posts = query.paginate(per_page=7, error_out=True)
    title = nav.nav_name

    prev_url = url_for('navigationBar', nav_url=n_url, page=posts.prev_num)
    next_url = url_for('navigationBar', nav_url=n_url, page=posts.next_num)

    return render_template('public/index.html', title=title, posts=posts,
                            prev_url=prev_url, next_url=next_url)


@app.route("/category-<cat_name>")
def cat(cat_name):
    cat = Category.query.filter_by(cat_name=cat_name).first()
    c_url = cat.cat_id
    # query = cat.post
    # posts = [m for m in query]
    post = Post.query.order_by(Post.post_id.desc())
    #query = cat.post.order_by(Post.post_id.desc())
    posts = post.paginate(per_page=7, error_out=True)
    title = cat.cat_name

    prev_url = url_for('cat', cat_name=cat_name, page=posts.prev_num)
    next_url = url_for('cat', cat_name=cat_name, page=posts.next_num)

    return render_template('public/index.html', title=title, posts=posts,
                            prev_url=prev_url, next_url=next_url)


@app.route('/rxdomain', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for("dashboard"))
    title = 'Login Page'
    if request.method == "POST":
        user = Admin.query.filter_by(admin_email=request.form["email"]).first()
        pw_verify = bcrypt.check_password_hash(user.password, request.form["password"])
        if user and pw_verify :
            login_user(user, remember=request.form.get('remember'))
            next_page = request.args.get('next')
            flash("you are successfully logged in!", "success")
            return redirect(next_page) if next_page else redirect(url_for('dashboard'))
        else:
            flash("invalid Login Details", "danger")
    return render_template('public/login.html', title=title)


# validation of remember token
@login_manager.user_loader
def load_user(user_id):
    return Admin.query.get(int(user_id))


@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for("home"))


@app.route('/reset-password')
def forgot():
    title = 'Password Reset'
    return render_template('public/forgot.html', title=title)


@app.errorhandler(404)
def page_not_found(e):
    title = '404 page not found'
    return render_template('404.html', title=title), 404

@app.errorhandler(500)
def page_not_found(e):
    title = 'Internal Server Error'
    return render_template('404.html', title=title), 500
