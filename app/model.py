from app import db, login_manager
from flask_login import UserMixin


postCats = db.Table('postCats',
    db.Column('post_id', db.Integer, db.ForeignKey('post.post_id')),
    db.Column('cat_id', db.Integer, db.ForeignKey('category.cat_id'))
)

class Category(db.Model):
    cat_id = db.Column(db.Integer, primary_key=True)
    cat_name = db.Column(db.String(20), unique=True, nullable=False)
    cat_url = db.Column(db.String(20), unique=True, nullable=False)
    date_added = db.Column(db.DateTime)
    post = db.relationship("Post", secondary=postCats, backref=db.backref("category", lazy='dynamic'))

    def __init__(self, cat_name, cat_url, date):
        self.cat_name = cat_name
        self.cat_url = cat_url
        self.date_added = date


class Navigation(db.Model):
    nav_id = db.Column(db.Integer, primary_key=True)
    nav_name = db.Column(db.String(50), unique=True, nullable=False)
    nav_url = db.Column(db.String(50), unique=True, nullable=False)
    nav_dd = db.Column(db.String(30), nullable=False)
    nav_ref = db.Column(db.String(50))
    date_added =  db.Column(db.DateTime)
    post = db.relationship("Post", backref="navigation", lazy=True)

    def __init__(self, name, url, dd, ref, date):
        self.nav_name = name
        self.nav_url =url
        self.date_added = date
        self.nav_dd = dd
        self.nav_ref = ref


class Admin(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    admin_full_name = db.Column(db.String(20))
    admin_username = db.Column(db.String(20), unique=True, nullable=False)
    admin_email = db.Column(db.String(50), unique=True, nullable=False)
    password = db.Column(db.String(100))
    post = db.relationship("Post", backref="author", lazy=True)

    def __init__(self, name, username, email, password):
        self.admin_full_name = name
        self.admin_username = username
        self.admin_email = email
        self.password = password


class Post(db.Model):
    post_id = db.Column(db.Integer, primary_key=True)
    post_author = db.Column(db.Integer, db.ForeignKey("admin.id"))
    post_url = db.Column(db.String(50), nullable=False, unique=True)
    post_title = db.Column(db.String(255), nullable=False)
    featured_img = db.Column(db.String(255))
    post_content = db.Column(db.Text)
    post_summary = db.Column(db.Text)
    post_date = db.Column(db.DateTime)
    post_nav = db.Column(db.Integer, db.ForeignKey("navigation.nav_id"))
    status = db.Column(db.String(50))
    comment = db.relationship("Comment", backref="post", lazy=True)

    def __init__(self, author, url, title,featured_img, content, summary, date, nav, status):
        self.post_author = author
        self.post_url = url
        self.post_title = title
        self.featured_img = featured_img
        self.post_content = content
        self.post_summary = summary
        self.post_date = date
        self.post_nav = nav
        self.status = status


class Comment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30), nullable=False)
    email = db.Column(db.String(30), nullable=False)
    content = db.Column(db.Text, nullable=False)
    date = db.Column(db.DateTime)
    post_id = db.Column(db.Integer, db.ForeignKey("post.post_id"))

    def __init__(self, name, email, content, post, date):
        self.name = name
        self.email = email
        self.content = content
        self.post_id = post
        self.date = date
