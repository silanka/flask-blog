from app import db
from app.model import Post 
import os


def delPhotos(img_path, content):
    '''this helps to delete uploaded images in a post by making a list
        of the their filename and checking if the file exist'''
    del_list =[]
    for i in range(len(content)):
        if (content[i] == 's') and (content[i + 1] == 'r') and (content[i + 2] == 'c') and (content[i + 3] == '='):
            j = i + 12
            start = content[j::]
            file_del = start.split('.')[0]
            del_list.append(file_del)
        if del_list:
            exts = ['jpeg', 'png', 'jpg', 'gif']
            for img in del_list:
                file_path = os.path.join(img_path, img)
                for ext in exts:
                    if os.path.isfile(file_path + '.' + ext):
                        os.remove(file_path + '.' + ext)
        del_list = []



# this nexts or previous posts
def prevPost(model, pid):
    count = model.query.count()
    while (count > 0):
        prev_model = None
        pid = pid + 1
        if model.query.get(pid):
            prev_post = model.query.get(pid)
            return prev_post        
            break
        else:
            count = count - 1
    

def nextPost(model, pid):
    count = model.query.count()
    while (count > 0):
        next_model = None
        pid = pid - 1
        if model.query.get(pid):
            next_post = model.query.get(pid)
            return next_post
            break
        else:
            count = count - 1

#  formatting url   
def formatPostUrl(post_url):
    formattedUrl = post_url.replace(' ', '-')
    return formattedUrl
