
from app import app
from app.model import db, Admin, Post, Category, Comment
from flask import render_template, url_for, redirect, request, flash
from flask_login import login_required


@app.route('/admin/dashboard')
@login_required
def dashboard():
    title = 'Admin Dashboard'
    admin_count = Admin.query.count()
    post_count = Post.query.count()
    cat_count = Category.query.count()
    latest_posts = Post.query.order_by(Post.post_id.desc())[:3]
    latest_comments = Comment.query.order_by(Comment.id.desc())[:10]
    return render_template('admin/dashboard.html', title=title,
                admin_count=admin_count, post_count=post_count,
                cat_count=cat_count, latest_posts=latest_posts,
                latest_comments=latest_comments)
