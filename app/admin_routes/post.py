
import os
import uuid
from PIL import Image
from app import app, db
from flask import render_template, url_for, redirect, request, flash, jsonify
from flask import send_from_directory
from app.model import db, Post, Admin, Navigation, Category, Comment, postCats
from datetime import datetime
from flask_ckeditor import CKEditor, upload_fail, upload_success
from flask_wtf import CSRFProtect
from flask_login import login_required
from werkzeug.utils import secure_filename

# importing my own module
from app.admin_routes.func import delPhotos, formatPostUrl

basedir = os.path.abspath(os.path.dirname(__file__))
app.config['CKEDITOR_SERVE_LOCAL'] = True
app.config['CKEDITOR_HEIGHT'] = 400
app.config['CKEDITOR_FILE_UPLOADER'] = 'upload'
app.config['CKEDITOR_ENABLE_CSRF'] = True
app.config['UPLOADED_PATH'] = os.path.join(basedir, '../static/uploads')

ckeditor = CKEditor(app)
csrf = CSRFProtect(app)


@app.route('/admin/new-post', methods=["GET", "POST"])
@app.route('/admin/new-post/<id>', methods=["GET", "POST"])
@login_required
def newPost(id=''):
    title = 'Admin New Post'
    # this pulls information from the admin and category table made
    # available in the respective section in the add post form
    admin_data = Admin.query.all()
    categories_data = Category.query.all()
    navigations_data = Navigation.query.all()
    # creating the add and add post process
    if request.method == "POST":
        rec = request.form
        author = rec['post_author']
        url = rec['post_url']
        formatted_url = formatPostUrl(url)
        title = rec['post_title']
        content = rec['ckeditor']
        nav = rec['post_nav']
        cats = rec.getlist('post_cat')
        status = rec['status']
        summary = rec['summary']

        date = datetime.now()
        if request.files:
            #image_name = request.cookies.get('filename')
            image_file = request.files['featured']
            if image_file:
                output_size = (750, 300)
                img_file = Image.open(image_file)
                img_file.thumbnail(output_size)

        if rec['id']:
            # this processes the edit post function
            edit_id = rec['id']
            post = Post.query.get(edit_id)
            post.post_author = author
            post.post_url = formatted_url
            post.post_title = title
            post.post_content = content
            post.post_summary = summary
            post.post_nav = nav
            post.post_date = date
            if image_file:
                img_old = os.path.join(app.config['UPLOADED_PATH'], post.featured_img)
                if os.path.isfile(img_old):
                    os.remove(img_old)
                post.featured_img = image_file.filename
            post.status = status
            message = "Post was successfully updated"
        else:
            # this processes the add post function
            post = Post(author, formatted_url, title, image_file.filename, content, summary, date, nav, status)
            db.session.add(post)
            
            message = "New Post was added successfully"
        # looping through the checked boxes to add the relationship between post and category in the postcats table
        if post.category:
            post.category = []
        for cat in cats:
            post.category.append(Category.query.get(cat))
        #save image_file here
        if image_file:
            img_file.save(os.path.join(app.config['UPLOADED_PATH'], image_file.filename))
        db.session.commit()
        flash(message)
        return redirect(url_for("viewPost"))
    elif request.method == 'GET':
        edit = Post.query.get(id)
        # author.data = edit.post_author

    return render_template('admin/new_post.html', title=title,
                           admins=admin_data, categories=categories_data,
                           edit=edit, navigations=navigations_data)


@app.route('/admin/viewpost')
@login_required
def viewPost():
    title = 'Admin Post'
    # querying the database for post information
    data = Post.query.order_by(Post.post_id.desc())
    return render_template('admin/view_post.html', title=title,
                            posts=data)


@app.route('/admin/viewpost-<del_id>')
@login_required
def post_del(del_id):
    # for deleting post based on their id in the database
    post = Post.query.get(del_id)
    Comment.query.filter_by(post_id=del_id).delete()
    db.session.delete(post)
    # deleting img file from uploads folder. function stored in func.py
    delPhotos(app.config['UPLOADED_PATH'], post.post_content)
    img_file = os.path.join(app.config['UPLOADED_PATH'], post.featured_img)
    print("img url : " + img_file)
    if os.path.isfile(img_file):
        os.remove(img_file)
    db.session.commit()
    flash('Category deleted successfully', "danger")
    return redirect(url_for("viewPost"))


#deleting featured image upload
#@app.route('/admin/new-post/<del_id>')
@app.route('/delFeatured', methods=["GET", "POST"])
@login_required
def del_featured_img():
    # data sent to server from new_post.html
    del_id =request.args.get("post_id")
    src = request.args.get("src")
    # splitting the src json file to get only the filename
    filename = src.split('/')[3]

    post =Post.query.get(del_id)
    img_file = os.path.join(app.config['UPLOADED_PATH'], filename)
    print("img url : " + img_file)
    if os.path.isfile(img_file):
        os.remove(img_file)
        post.featured_img = ""
        db.session.commit()
    return jsonify(status="success")




@app.route('/files/<filename>')
@login_required
def uploaded_files(filename):
    path = app.config['UPLOADED_PATH']
    return send_from_directory(path, filename)


@app.route('/upload', methods=['POST'])
@login_required
def upload():
    f = request.files.get('upload')
    extension = f.filename.split('.')[1].lower()
    if extension not in ['jpg', 'gif', 'png', 'jpeg']:
        return upload_fail(message='Image only!')
    if os.path.isfile(os.path.join(app.config['UPLOADED_PATH'], f.filename)):
        unique = uuid.uuid4().hex
        f.filename = unique + '.' + extension
    f.filename = secure_filename(f.filename)
    f.save(os.path.join(app.config['UPLOADED_PATH'], f.filename))
    url = url_for('uploaded_files', filename=f.filename)
    return upload_success(url=url)