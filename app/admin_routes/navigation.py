
from app import app, db
from flask import render_template, url_for, redirect, request, flash
from app.model import Navigation
from datetime import datetime
from flask_login import login_required


@app.route('/admin/navigation', methods=['GET', 'POST'])
@login_required
def navigation():
    title = 'Admin navigation'
    dropdowns = Navigation.query.filter_by(nav_dd='Yes').all()
    if request.method == 'POST':
        nav = request.form
        nav_name = nav['nav_name']
        nav_url = nav['nav_url']
        nav_dd = nav['nav_dd']
        nav_ref = nav['nav_ref']
        nav_date = datetime.now()
        

        if nav['nav_id'] != '':
            # this process edits a selected navigation
            data_id = nav['nav_id']
            nav = Navigation.query.filter_by(nav_id=data_id).one()
            nav.nav_name = nav_name
            nav.nav_url = nav_url
            nav.nav_dd = nav_dd
            nav.nav_ref = nav_ref
            nav.date_added = nav_date
            message = 'navigation was updated successfully'
        else:
            # this process adds new navigation
            nav = Navigation(nav_name, nav_url,nav_dd, nav_ref, nav_date)
            db.session.add(nav)
            message = 'navigation was added successfully'
        db.session.commit()
        flash(message, "success")
        return redirect(request.url)

    # this process retrieves data from database and displays it to the user
    data = Navigation.query.all()
    return render_template('admin/navigation.html',
                           navigations=data, title=title, dropdowns=dropdowns)


@app.route('/admin/navigation/del-<string:del_id>')
@login_required
def del_navigation(del_id):
    # this deletes a navigation
    data = Navigation.query.get(del_id)
    db.session.delete(data)
    db.session.commit()
    flash('Navigation deleted successfully', "danger")
    return redirect(url_for('navigation'))
