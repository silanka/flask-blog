
import os
from app import app, manager
from app.model import Category, Navigation, Comment,Post
from flask import url_for

# admin and site view importation
from app import views
from app import admin_views
from app.views import cat

@app.context_processor
def context_processor():
    return dict(
                categories=Category.query.all(),
                navigations=Navigation.query.order_by(Navigation.nav_id.asc()),
                comment_count=Comment.query.count(),
                cat=cat,
                post = Post.query.all(),
                popular_posts = Post.query.all()
                )

if __name__ == '__main__':
    manager.run()
